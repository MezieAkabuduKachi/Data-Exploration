# Data-Exploration

Hello and welcome to my data analysis portfolio project! In this project, I leveraged my SQL skills to perform data exploration and analysis on a database that I previously created. I utilized various statistical techniques and visualizations to gain insights into the data and draw meaningful conclusions. The project report provides a detailed explanation of my approach and findings. 
